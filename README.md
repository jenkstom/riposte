Riposte (n): A snappy comeback.
Re-post (v): The bane of medics everywhere.

A project to provide system status management for EMS agencies.

This is written in Django and GeoDjango with python for task scripting. It 
utilizes PostgreSQL with PostGis and pgRouting to manage geospatial information.

This project is in very early development, with only basic prototyped
functionality.

Here are the components:

1. Response Time Management
    * Graphs showing hotspots
    * Unit management
    * Suggested unit movements
        * Thermodynamic algorithm
        * Flocking algorithm

2. Traffic Congestion Management
    * Build unlimited traffic congestion models
    * Multiple congestion models can be scheduled for hourly overall models

3. Demand Management
    * Model multiple types of demand per grid point
    * Designate unit response types for demand types
    * Model population flows on hourly basis
    * Patient transport models 
        * Divert
        * Transport time

4. Dispatch Management
    * Dispatch system based on NIMS
    * Assign unlimited resources and sub-events to response event
    * Simple personnel system

5. Basic finance
    * Simple journalling system for integration to external GL

These are intended to be built in phases. Components 1 and 2 are the main focus
and the rest are speculative at this point.

![Screenshot 1](responsecap1.png)
