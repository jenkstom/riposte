from django.db import models
from django.contrib.gis.db import models

#personnel


class PersonType(models.Model):
  """
  A type of person such as: employee, contractor, volunteer, etc.
  """
  typedescription = models.CharField("Description of person type")
  #cost_class
  #


class Person(models.Model):
  """Any person used as a resource. An employee, volunteer, etc."""
  name = models.CharField("Person name", null=False)
  persontype = models.ForeignKey(PersonType)

  created = models.DateTimeField("Record creation", auto_created=True)
  updated = models.DateTimeField("Record updated", auto_now=True)


class ShiftType(models.Model):
  """
  A shift type, such as paramedic, emt, supervisor, ICS position, etc.
  """
  description = models.CharField("Shift type description")
  #gl / cost center / etc