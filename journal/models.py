from django.db import models
from django.contrib.gis.db import models

#GL journal interface mix-in

class Journalled(models.Model):
  """
  Add fields for posting to GL
  """
  journalflag = models.BooleanField("Record written to journal flag", default=False)
  journaldate = models.DateTimeField("Date and time record written to journal", null=True)


class JournalSetup(models.Model):
  """
  Information about how various tables are modelled

  Table name

  """


class Disbursements(models.Model):
  """
  How journalled items are disbursed.
  Rules

  No real clue how to do this
  """
  setup = models.ForeignKey(JournalSetup)