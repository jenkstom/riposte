from django.db import models
from django.contrib.gis.db import models
from personnel.models import *
from journal.models import *

class IncidentType(models.Model):
  description = models.CharField("Incident type description")
  #costclass, gl, whatever


class Incident(Journalled):
  """
  Incident as per National Incident Management System

  May have multiple personnel, multiple positions, plans, resources, etc

  """
  created = models.DateTimeField("Incident creation", auto_created=True)
  updated = models.DateTimeField("Last update", auto_now=True)


class IncidentPersonShift(Journalled):
  """
  People associated with an incident by shift, with title, time, more
  """
  person = models.ForeignKey(Person)
  start = models.DateTimeField("Time shift start")
  finish = models.DateTimeField("Time shift end")
  #position = models.ForeignKey()