#!/usr/bin/env python3

import psycopg2
import random
import os

#create random units in area, then update road network and master response grid

minlong = -95.8
maxlong = -96.05
minlat = 35.9
maxlat = 36.2
schema = os.environ["FRDB_SCHEMA_NAME"]
user = os.environ["FRDB_USERNAME"]
pword = os.environ["FRDB_PASSWORD"]
host = os.environ["FRDB_HOST"]
port = os.environ["FRDB_PORT"]

try:
  conn = psycopg2.connect(database=schema, user=user, password=pword, host=host, port=port)
except:
  print("Can't connect to database")
  exit()

cur = conn.cursor()
cur.execute("delete from responsetime_unit; " \
            "delete from responsetime_unitnetwork;" \
            "update responsetime_responsegrid set mintime=-1;" \
            "update responsetime_responsegrid rg set mintime=99 " \
            "from responsetime_roadnetwork rn " \
            "join responsetime_grid g on g.poly && rn.geom " \
            "where rg.gridpoint_id=g.id")


for i in range(1, 11):
  x = random.uniform(minlong, maxlong)
  y = random.uniform(minlat, maxlat)

  sql = "insert into responsetime_unit (area_id,descriptor,geom,netgeom) VALUES " \
        "(1,'Unit {0}',st_setsrid(st_pointfromtext('POINT({1} {2})'),4326), " \
        "st_setsrid(st_pointfromtext('POINT({1} {2})'),4326))".format(i, round(x, 4), round(y, 2))
  print(sql)

  cur.execute(sql)

  sql = "insert into responsetime_unitnetwork (sequence,cost,roadsegment_id,unitid_id) " \
        "select seq, a.cost, gid, (select id from responsetime_unit " \
        "where descriptor='Unit {0}') from pgr_drivingDistance(" \
        "'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost " \
        "FROM responsetime_roadnetwork where area_id=1 order by gid'," \
        "nn((select geom from responsetime_unit where descriptor='Unit {0}'), " \
        "0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 10, true, true) a " \
        "JOIN responsetime_roadnetwork o ON a.id1 = o.source order by seq".format(i)
  print(sql)
  cur.execute(sql)


  sql = "update responsetime_responsegrid rg set mintime=u.cost " \
        "from responsetime_unitnetwork u join responsetime_roadnetwork n " \
        "on roadsegment_id=gid join responsetime_grid g on g.poly && n.geom " \
        "where (mintime>u.cost or mintime<0) and g.id=rg.gridpoint_id and unitid_id= " \
        "(select id from responsetime_unit where descriptor='Unit {0}')".format(i)
  print(sql)
  cur.execute(sql)

  conn.commit()

conn.autocommit=True;
cur.execute("vacuum analyze")
cur.close()
conn.close()