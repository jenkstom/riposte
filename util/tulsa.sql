--This is the old scratch file, most of it is garbage

--setup postgresql, postgis and pgrouting
--add-apt-repository ppa:georepublic/pgrouting-unstable
--apt-get install postgresql postgis osm2pgrouting pgrouting-workshop postgresql-9.3-pgrouting
--create tables - imposm2 + osm2po works very well
--use qgis to view outputs

--Create tulsa_2po_4pgr from open street maps data and osm2po
--Some utility functions are at the end of this file
--for idempotent runs the functions need to be moved to the top

--next steps:
--  Create procedure that calculates to nearest hospitals given
--  Create a way to do a "slowdown" to simulate area problems or rush hour or other things

--then dynamic side
--  Table for current unit positions
--  Predicted call volume by grid point table
--  Need a new function to create response time to grid points in realtime (use sparser grid?)


--drop table tulsagrid
create table tulsagrid (id serial primary key, gcol int, grow int, district int, nearestroad int, nrpos double precision);
SELECT AddGeometryColumn('', 'tulsagrid','geom',-1,'POINT',2);
--select DropGeometryColumn('','tulsagrid','geom');
insert into tulsagrid (gcol,grow,geom) SELECT gcol, grow,st_setsrid(geom,4326) AS geomWKT
FROM ST_RegularGrid(ST_GeomFromText('LINESTRING(-96.39999 35.750038, -95.400063 36.4098387)',4326),0.005,0.005);
select count(*) from tulsagrid;
SELECT UpdateGeometrySRID('tulsagrid','geom',4326);
CREATE INDEX tulsagrid_geom
  ON public.tulsagrid
  USING gist
  (geom);

--delete from tulsagrid;
select * from tulsagrid;

-- In qgis to create hex grid -96.39999, -95.400063,35.750038, 36.4098387, 0.005
select count(*) from tulsagrid;
select count(*) from tulsagrid_poly;
select * from tulsagrid_poly h join tulsagrid g on h.geom && g.geom;
delete from tulsagrid_poly where id not IN
(select h.id from tulsagrid_poly h join tulsagrid g on h.geom && g.geom);

update tulsagrid_poly p set id=
(select id from tulsagrid g where st_within(st_setsrid(g.geom,4326),st_setsrid(p.geom,4326)));
--update tulsagrid_poly set id=id+100000;

--response grid
--drop table tulsargrid
create table tulsargrid (id serial primary key, gridpoint int, unit int, rtime numeric);
SELECT AddGeometryColumn('', 'tulsargrid','geom',-1,'POINT',2);
create table tulsadgrid (id serial primary key, gridpoint int, probability numeric);

select * from tulsargrid where unit=1 and rtime>0;

--drop table tulsa_units
create table tulsa_units
(
  id serial primary key,
  unitid varchar(10),
  unitdesc varchar(200),
  unitstatus char(1),
  nearestroad int,
  nrpos double precision
);
SELECT AddGeometryColumn('', 'tulsa_units','geom',-1,'POINT',2);

insert into tulsa_units (unitid, geom) values
  ('Unit 1', st_setsrid(ST_PointFromText('POINT(-95.988955 36.226597)'),4326));

select * from generate_series(2,5);
select * from generate_series(0,100,5);
select * from generate_series(37881,37891);
select * from tulsa_2po_4pgr order by km desc limit 1;
select geom_way, st_length(geom_way), (0.005 / st_length(geom_way)*100)
from tulsa_2po_4pgr where id in (37881,37882,37883,37884,37885,37886,37891);
select postgis_version();
select pgr_version();

select * from tulsa_units;
select update_unit(1, st_setsrid(ST_PointFromText('POINT(-95.988955 36.226597)'),4326));

select * from tulsargrid r join tulsagrid_poly p on r.gridpoint=p.id;
select max(rtime) from tulsargrid;
SELECT UpdateGeometrySRID('tulsagrid_poly','geom',4326);

update tulsargrid rg set rtime=
  (select rtime from tulsargrid rg2 where rg2.gridpoint =
  (select id from tulsagrid g order by st_distance(g.geom,(select geom from tulsagrid g2 where g2.id=rg.gridpoint)) desc limit 1)
  and rg2.gridpoint=rg.gridpoint)
where rg.unit=1 and (rtime is null or rtime=0);

update tulsargrid rg set rtime=
  (select rtime from tulsargrid rg2 where rg2.gridpoint =
  nn((select geom from tulsagrid g where id=rg.gridpoint), 0.005, 2, 20, 'tulsagrid', 'id', 'geom')
  and rtime>0)
where rg.unit=1 and (rtime is null or rtime=0);

select
  nn((select geom from tulsagrid g where id=rg.gridpoint), 0.005, 2, 20, 'tulsagrid', 'id', 'geom'), rg.*
from tulsargrid rg
where rg.unit=1 and (rtime is null or rtime=0);

select count(*) from tulsargrid where (rtime=0 or rtime is null);

select rtime from tulsargrid rg2 where rg2.gridpoint =
  (select id from tulsagrid g order by st_distance(g.geom,(select geom from tulsagrid g2 where g2.id=rg2.gridpoint)) limit 1);
--
CREATE OR REPLACE FUNCTION update_unit(unitid int,location geometry)
  RETURNS integer AS
  $BODY$
--Declarations
DECLARE
    r record;
    s record;
    i integer;
    rc integer;
    gc integer;
    gpoint integer;
    sql     text;
    result  integer;
BEGIN
    --Update unit table, then delete and create an rgrid for this unit
    rc := (select count(*) from tulsargrid where unit=unitid);
    gc := (select count(*) from tulsagrid);

    --if there are no response grid points or the wrong number of points then (re)create them
    IF rc<>gc THEN
      delete from tulsargrid where unit=unitid;
      insert into tulsargrid (gridpoint, unit, rtime, geom) select id, unitid, 0, geom from tulsagrid;
    else
      update tulsargrid set rtime=null where id=unitid;
    END IF;

    i := 0;
    FOR r IN
      select seq, id1, id2, a.cost, id, osm_id, osm_name,
        geom_way, st_length(geom_way) as len, km, o.cost as segcost
      from pgr_drivingDistance(
      'SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id',
      (select source from tulsa_2po_4pgr order by st_distance(geom_way,location) limit 1), 10, false, false) a
      JOIN tulsa_2po_4pgr o ON a.id2 = o.id
    LOOP
        update tulsargrid set rtime=r.cost
        where gridpoint in
          (select id from tulsagrid_poly where st_intersects(geom,r.geom_way) and geom && r.geom_way)
          and (rtime>r.cost or rtime=0) and unitid=$1;

        i := i + 1;

    END LOOP;

    update tulsa_units set geom=location where id=$1;
    RETURN i;
END;
$BODY$
LANGUAGE plpgsql;


vacuum;

-------------------

select seq, id1, id2, a.cost, id, osm_id, osm_name,
geom_way, st_length(geom_way) as len, km, o.cost as segcost
from pgr_drivingDistance(
'SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id',
(select source from tulsa_2po_4pgr order by st_distance(geom_way,st_setsrid(ST_PointFromText('POINT(-95.988955 36.226597)'),4326)) limit 1), 10, false, false) a
JOIN tulsa_2po_4pgr o ON a.id2 = o.id;

select seq, id1, id2, a.cost, id, osm_id, osm_name, geom_way, st_length(geom_way) as len, km,st_length(geom_way) / 0.005
from pgr_drivingDistance('SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id',
  (select source from tulsa_2po_4pgr where id=9941), 10, false, false) a
JOIN tulsa_2po_4pgr o ON a.id2 = o.id;



--drop table tulsa_dests
create table tulsa_dests (
destid serial primary key,
destname varchar(200),
geom geometry,
nearestroad int,
nrpos double precision
);

select st_astext(geom_way) from tulsa_2po_4pgr where id=9941;

--set nearest segment to each grid point VERY SLOW (remove where g.id=1 to do all)
update tulsagrid g set nearestroad =
(SELECT id FROM tulsa_2po_4pgr r ORDER BY ST_Distance(r.geom_way,ST_SetSRID(g.geom,4326)) ASC LIMIT 1)
where g.id=1;

--find nearest point on nearest line to grid point
update tulsagrid gs set nrpos=
(SELECT ST_Line_Locate_Point(
    (SELECT geom_way FROM tulsa_2po_4pgr WHERE id = g1.nearestroad),
    st_setsrid(geom, 4326))
FROM tulsagrid g1
WHERE g1.id = gs.id);

--set nearest segment to each grid point VERY SLOW (remove where g.id=1 to do all)
update tulsagrid g set nearestroad =
(SELECT id FROM tulsa_2po_4pgr r ORDER BY ST_Distance(r.geom_way,ST_SetSRID(g.geom,4326)) ASC LIMIT 1)
where g.id=1;

--find nearest point on nearest line to grid point
update tulsagrid gs set nrpos=
(SELECT ST_Line_Locate_Point(
    (SELECT geom_way FROM tulsa_2po_4pgr WHERE id = g1.nearestroad),
    st_setsrid(geom, 4326))
FROM tulsargrid g1
WHERE g1.id = gs.id);




--We need a many to many table linking the grids to hospitals
--this is where we put the time from each grid point to each hospital
create table tulsa_ttd (
runid int, gridid int, destid int, time_to_dest double precision
);

--drop table gridruns
create table gridruns (
runid serial,
runtime timestamp default current_timestamp,
runcomment varchar(999)
);

insert into tulsa_dests(destname, geom) values ('Saint Francis ER', ST_GeomFromText('POINT(-95.918 36.068)',0))


update tulsa_dests d set nearestroad =
(SELECT id FROM tulsa_2po_4pgr r ORDER BY ST_Distance(r.geom_way,ST_SetSRID(d.geom,4326)) ASC LIMIT 1);

update tulsa_dests ds set nrpos=
(SELECT ST_Line_Locate_Point(
    (SELECT geom_way FROM tulsa_2po_4pgr WHERE id = d1.nearestroad),
    st_setsrid(geom, 4326))
FROM tulsa_dests d1
WHERE d1.destid = ds.destid);

select * from tulsa_dests;
select * from tulsagrid where id=1;
select tulsa_getcost(9041, 0.520, 59139, 0.481);
select tulsa_getcost(9041, 0.520, 1, 0.481);

select sum(a.cost) FROM pgr_astar(
 'SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id' ,
 (select source from tulsa_2po_4pgr where id=9941),(select target from tulsa_2po_4pgr where id=59139),
 true, true) a
JOIN tulsa_2po_4pgr o ON a.id2 = o.id;

select pgr_alphashape('select seq as id, x1 as x, y1 as y
from pgr_drivingDistance(''SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id'',
  (select source from tulsa_2po_4pgr where id=9941), 0.2, false, false) a
JOIN tulsa_2po_4pgr o ON a.id2 = o.id');


SELECT 1 as id, ST_MakePolygon(ST_AddPoint(foo.openline, ST_StartPoint(foo.openline))) as poly
from (select st_makeline(points order by id)  as openline from
(SELECT st_makepoint(x,y) as points ,row_number() over() AS id
FROM pgr_alphShape('select seq as id, geom_way
from pgr_drivingDistance(''SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id'',
  (select source from tulsa_2po_4pgr where id=9941), 10, false, false) a
JOIN tulsa_2po_4pgr o ON a.id2 = o.id where a.cost<0.5')
) as a) as foo;


--alpha shape
SELECT 1 as id, ST_MakePolygon(ST_AddPoint(foo.openline, ST_StartPoint(foo.openline))) as poly
from (select st_makeline(points order by id)  as openline from
(SELECT st_makepoint(x,y) as points ,row_number() over() AS id
from pgr_alphaShape('with s1 as (select seq, id1, id2, a.cost, id, osm_id, osm_name, geom_way
from pgr_drivingDistance(''SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id'',
  (select source from tulsa_2po_4pgr where id=9941), 10, false, false) a JOIN tulsa_2po_4pgr o ON a.id2 = o.id where a.cost<0.05)
  select seq*2 as id, st_x(st_startpoint(geom_way)) as x, st_y(st_startpoint(geom_way)) as y from s1
  union select seq*2+1 as id, st_x(st_endpoint(geom_way)) as x, st_y(st_endpoint(geom_way)) as y from s1')
) as a) as foo;

--convex hull 	ST_ConvexHull(ST_Collect(somepointfield))
with s1 as (select seq, id1, id2, a.cost, id, osm_id, osm_name, geom_way
from pgr_drivingDistance('SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id',
  (select source from tulsa_2po_4pgr where id=9941), 10, false, false) a JOIN tulsa_2po_4pgr o ON a.id2 = o.id where a.cost<0.05)
select ST_ConvexHull(ST_Collect(geom_way)) from s1;



  select * from pgr_alphaShape('select seq*2 as id, st_x(st_startpoint(geomway)) as x, st_y(st_startpoint(geomway)) as y
  union select seq*2+1 as id, st_x(st_endpoint(geomway)) as x, st_y(st_endpoint(geomway)) as y');


select * from tulsagrid g join tulsa_ttd t on g.id=t.gridid where runid=6;
select avg(time_to_dest) from tulsa_ttd;
select count(*) from tulsa_ttd where runid=6 and time_to_dest>1;
update tulsa_ttd set time_to_dest=null where runid=6 and time_to_dest>1;

select tulsa_seqrouting(1,'Test run #1');

SELECT seq, osm_id, osm_name, geom_way
FROM pgr_astar(
         'SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id' ,
         nn(st_setsrid(ST_PointFromText('POINT(-96.35 36.28)'), 4326), 0.01, 2, 100, 'tulsa_2po_4pgr', 'source', 'geom_way'),
         nn(st_setsrid(ST_PointFromText('POINT(-95.92 36.07)'), 4326), 0.01, 2, 100, 'tulsa_2po_4pgr', 'target', 'geom_way'), true, true) r
  JOIN tulsa_2po_4pgr o ON r.id2 = o.id;

update tulsa_ttd set time_to_dest = tulsa_getcost(
(select nearestroad from tulsagrid where id=17351),
(select nrpos from tulsagrid where id=17351),
(select nearestroad from tulsa_dests where destid=1),
(select nrpos from tulsa_dests where destid=1)) where runid=6 and gridid=17351;

select tulsa_getcost(
(select nearestroad from tulsagrid where id=17351),
(select nrpos from tulsagrid where id=17351),
(select nearestroad from tulsa_dests where destid=1),
(select nrpos from tulsa_dests where destid=1));

select nearestroad from tulsagrid where id=17351;

--
CREATE OR REPLACE FUNCTION tulsa_seqrouting(rundestid int,comment varchar(99))
  RETURNS integer AS
  $BODY$
--Declarations
DECLARE
    r record;
    i integer;
    thisrunid integer;
    map_id2 integer;
    map_pos2 double precision;
BEGIN
    --CODE to calculate routes and update table
    insert into gridruns (runcomment) values ((select destname from tulsa_dests where destid=rundestid)||' - '||comment);
    thisrunid := (select max(runid) from gridruns);
    map_id2 := (select nearestroad from tulsa_dests where destid=rundestid);
    map_pos2 := (select nrpos from tulsa_dests where destid=rundestid);

    i := 0;
    FOR r IN
      SELECT  id            AS gridid,
        nearestroad         AS map_id1,
        nrpos               AS map_pos1
      FROM tulsagrid
    LOOP

        insert into tulsa_ttd values (thisrunid, r.gridid, rundestid,
            tulsa_getcost(r.map_id1, r.map_pos1, map_id2, map_pos2));

        i := i + 1;

    END LOOP;
    RETURN i;
END;
$BODY$
LANGUAGE plpgsql;

--
CREATE OR REPLACE FUNCTION tulsa_rseqrouting(rundestid int,comment varchar(99))
  RETURNS integer AS
  $BODY$
--Declarations
DECLARE
    r record;
    i integer;
    thisrunid integer;
    map_id2 integer;
    map_pos2 double precision;
BEGIN
    --CODE to calculate routes and update table
    insert into gridruns (runcomment) values ((select destname from tulsa_dests where destid=rundestid)||' - '||comment);
    thisrunid := (select max(runid) from gridruns);
    map_id2 := (select nearestroad from tulsa_dests where destid=rundestid);
    map_pos2 := (select nrpos from tulsa_dests where destid=rundestid);

    i := 0;
    FOR r IN
      SELECT  id            AS gridid,
        nearestroad         AS map_id1,
        nrpos               AS map_pos1
      FROM tulsargrid
    LOOP

        insert into tulsa_ttd values (thisrunid, r.gridid, rundestid,
            tulsa_getcost(r.map_id1, r.map_pos1, map_id2, map_pos2));

        i := i + 1;

    END LOOP;
    RETURN i;
END;
$BODY$
LANGUAGE plpgsql;


--utility functions / types

create or replace function tulsa_getcost(edge1 integer, pos1 double precision, edge2 integer, pos2 double precision)
  returns double precision AS
$BODY$
DECLARE
  totalcost double precision;
  prevcost double precision;
  r record;
BEGIN
  --first segment use length1 * pos1
  --last segment use length2 * (1-pos2)
  --all other segments just add
  totalcost := 0;
  prevcost := 0;
  for r IN
  select *
    FROM pgr_astar(
         'SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id' ,
         (select source from tulsa_2po_4pgr where id=edge1),(select target from tulsa_2po_4pgr where id=edge2),
         true, true) a
    JOIN tulsa_2po_4pgr o ON a.id2 = o.id
  LOOP
    totalcost := totalcost + prevcost;

    if r.seq=0 THEN
      totalcost := totalcost + r.cost * pos1;
    else
      prevcost := r.cost;
    end if;
  END LOOP;

  totalcost := totalcost + prevcost * pos2;

  return totalcost;

  EXCEPTION
    WHEN SQLSTATE 'XX000' THEN RETURN NULL;
    WHEN SQLSTATE '38001' THEN RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql;


-- Create required type
DROP   TYPE IF EXISTS T_Grid CASCADE;
CREATE TYPE T_Grid AS (
  gcol  int4,
  grow  int4,
  geom geometry
);
-- Drop function is exists
DROP FUNCTION IF EXISTS ST_RegularGrid(geometry, NUMERIC, NUMERIC, BOOLEAN);
-- Now create the function
CREATE OR REPLACE FUNCTION ST_RegularGrid(p_geometry   geometry,
  p_TileSizeX  NUMERIC,
  p_TileSizeY  NUMERIC,
  p_point      BOOLEAN DEFAULT TRUE)
  RETURNS SETOF T_Grid AS
  $BODY$
    DECLARE
       v_mbr   geometry;
       v_srid  int4;
       v_halfX NUMERIC := p_TileSizeX / 2.0;
       v_halfY NUMERIC := p_TileSizeY / 2.0;
       v_loCol int4;
       v_hiCol int4;
       v_loRow int4;
       v_hiRow int4;
       v_grid  T_Grid;
    BEGIN
       IF ( p_geometry IS NULL ) THEN
          RETURN;
       END IF;
       v_srid  := ST_SRID(p_geometry);
       v_mbr   := ST_Envelope(p_geometry);
       v_loCol := trunc((ST_XMIN(v_mbr) / p_TileSizeX)::NUMERIC );
       v_hiCol := CEIL( (ST_XMAX(v_mbr) / p_TileSizeX)::NUMERIC ) - 1;
       v_loRow := trunc((ST_YMIN(v_mbr) / p_TileSizeY)::NUMERIC );
       v_hiRow := CEIL( (ST_YMAX(v_mbr) / p_TileSizeY)::NUMERIC ) - 1;
       FOR v_col IN v_loCol..v_hiCol Loop
         FOR v_row IN v_loRow..v_hiRow Loop
             v_grid.gcol := v_col;
             v_grid.grow := v_row;
             IF ( p_point ) THEN
               v_grid.geom := ST_SetSRID(
                                 ST_MakePoint((v_col * p_TileSizeX) + v_halfX,
                                              (v_row * p_TileSizeY) + V_HalfY),
                                 v_srid);
             ELSE
               v_grid.geom := ST_SetSRID(
                                 ST_MakeEnvelope((v_col * p_TileSizeX),
                                                 (v_row * p_TileSizeY),
                                                 (v_col * p_TileSizeX) + p_TileSizeX,
                                                 (v_row * p_TileSizeY) + p_TileSizeY),
                                 v_srid);
             END IF;
             RETURN NEXT v_grid;
         END Loop;
       END Loop;
    END;
    $BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100
ROWS 1000;
-- Assign ownership
ALTER FUNCTION st_regulargrid(geometry, NUMERIC, NUMERIC, BOOLEAN)
OWNER TO fleetres;


CREATE OR REPLACE FUNCTION tulsa_pgr2geom(edge1 integer, pos1 double precision, edge2 integer, pos2 double precision)
  RETURNS geometry AS
  $BODY$
--We have to do a routing query. And declare a cursor for it
DECLARE resc CURSOR FOR
SELECT * FROM pgr_astar (
    'SELECT id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM tulsa_2po_4pgr order by id',
    $1, $2, $3, $4, false, true);
doline geometry[];
temp_point geometry;
geom geometry;
temp_rec RECORD;
n integer;
BEGIN

--Append all the edges
FOR temp_rec IN SELECT * FROM pgr_astar (
    'SELECT * FROM tulsa_2po_4pgr',
    $1, $2, $3, $4, false, true) LOOP
        doline := array_append(
        doline, (SELECT map.geom_way FROM tulsa_2po_4pgr map WHERE map.id = temp_rec.id2));
END LOOP;
--Remove 1st and last edge
n := array_length (doline, 1);
doline := doline [2:n-1];
--Find startpoint and append to doline
doline := array_prepend(
    ST_LineInterpolatePoint((SELECT map.geom_way FROM tulsa_2po_4pgr map WHERE map.id = $1),$2),doline);
--Append the endpoint
doline := array_append(
    doline,ST_LineInterpolatePoint((SELECT map.geom_way FROM tulsa_2po_4pgr map WHERE map.id = $3),$4));
geom := ST_MakeLine(doline);
RETURN geom;
EXCEPTION
WHEN SQLSTATE 'XX000' THEN RETURN NULL;
WHEN SQLSTATE '38001' THEN RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
ALTER FUNCTION tulsa_pgr2geom(integer, double precision, integer, double precision)
OWNER TO gis;



--generic cleaned up versions of functions
--Stored procedure to create a time-to-destination
--grid to a single point in a city.
CREATE OR REPLACE FUNCTION
  seqrouting(rundestid int,comment varchar(99))
  RETURNS integer AS
$BODY$
DECLARE
  r         record;
  i         integer;
  thisrunid integer;
  map_id2   integer;
  map_pos2  double precision;
BEGIN
    insert into gridruns (runcomment) values
      ((select destname from dests where
        destid=rundestid)||' - '||comment);

    thisrunid := (select max(runid) from gridruns);

    map_id2 := (select nearestroad
                from dests
                where destid=rundestid);

    map_pos2 := (select nrpos from
                 dests where destid=rundestid);

    i := 0;
    FOR r IN
        SELECT  id      AS gridid,
            nearestroad AS map_id1,
            nrpos       AS map_pos1
        FROM grid
    LOOP

        insert into ttd values (thisrunid, r.gridid,
            rundestid,
            tulsa_getcost(r.map_id1, r.map_pos1, map_id2, map_pos2));

        i := i + 1;

    END LOOP;
    RETURN i;
END;
$BODY$
LANGUAGE plpgsql;


--scratch
select count(*) from tulsa_2po_4pgr;


select nn((select geom from responsetime_unit where "Descriptor"='GCVFD 34'), 0.005, 2, 20,'responsetime_roadnetwork','gid','geom');

select * from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM responsetime_roadnetwork where area_id=1 order by gid',
nn((select geom from responsetime_unit where "Descriptor"='GCVFD 34'), 0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 0.01, true, true);

select * from pgr_ways where source=93192;
select * from pgr_ways where gid=1;
select * from pgr_ways where gid=53304;

--select pgr_createTopology('pgr_ways', 0.001,'the_geom', 'gid','source','target');
--update pgr_ways set source=0, target=0;

select * from responsetime_grid where Area_id=1;

--Example for pgr_drivingdistance
select row_number() over (order by a.cost) as id, seq, a.cost, id1, id2, osm_id, name,
  geom, st_length(geom) as len, length, o.cost as segcost
from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM responsetime_roadnetwork where area_id=1 order by gid',
nn((select geom from responsetime_unit where "Descriptor"='GCVFD 34'), 0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 0.01, true, true) a
JOIN responsetime_roadnetwork o ON a.id1 = o.source order by seq;

--update unit

update responsetime_unit set
  netgeom=st_setsrid(st_pointfromtext('POINT (-95.79445 36.16423)'),4326),
  geom=st_setsrid(st_pointfromtext('POINT (-95.79445 36.16423)'),4326)
where descriptor = 'Unit 2';

select * from responsetime_unit;
delete from responsetime_unitnetwork where unitid_id=3;
insert into responsetime_unitnetwork (sequence,cost,roadsegment_id,unitid_id)
select seq, a.cost, gid, 2
from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM responsetime_roadnetwork where area_id=1 order by gid',
nn((select geom from responsetime_unit where descriptor='Unit 1'), 0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 10, true, true) a
JOIN responsetime_roadnetwork o ON a.id1 = o.source order by seq;

select row_number() over (order by u.cost), sequence as seq, u.cost,
  gid, geom
from responsetime_unitnetwork u
  join responsetime_roadnetwork g on roadsegment_id=gid;

select * from pgr_ways where gid=45698;
select * from responsetime_roadnetwork where oneway=1;

insert into responsetime_unit (area_id,descriptor,geom,netgeom) VALUES
  (1,'Unit 1',st_setsrid(st_pointfromtext('POINT(-95.95044 36.02632)'),4326),
   st_setsrid(st_pointfromtext('POINT(-95.95044 36.02632)'),4326));
insert into responsetime_unitnetwork (sequence,cost,roadsegment_id,unitid_id)
select seq, a.cost, gid, (select id from responsetime_unit where descriptor='Unit 1')
from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM responsetime_roadnetwork where area_id=1 order by gid',
nn((select geom from responsetime_unit where descriptor='Unit 1'), 0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 20, true, true) a
JOIN responsetime_roadnetwork o ON a.id1 = o.source order by seq;

insert into responsetime_unit (area_id,descriptor,geom,netgeom) VALUES
  (1,'Unit 2',st_setsrid(st_pointfromtext('POINT(-95.79445 36.16423)'),4326),
   st_setsrid(st_pointfromtext('POINT(-95.79445 36.16423)'),4326));
insert into responsetime_unitnetwork (sequence,cost,roadsegment_id,unitid_id)
select seq, a.cost, gid, (select id from responsetime_unit where descriptor='Unit 2')
from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM responsetime_roadnetwork where area_id=1 order by gid',
nn((select geom from responsetime_unit where descriptor='Unit 2'), 0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 20, true, true) a
JOIN responsetime_roadnetwork o ON a.id1 = o.source order by seq;
--delete from responsetime_unitnetwork where unitid_id = (select id from responsetime_unit where descriptor='Unit 2')
--delete from responsetime_unit

--sample data


--insert into responsetime_unit (area_id,descriptor,geom,netgeom) VALUES
--  (1,'GCVFD 34',st_setsrid(st_pointfromtext('POINT(-96.20912 36.2063)'),4326),
--   st_setsrid(st_pointfromtext('POINT(-96.20912 36.2063)'),4326));

--update unit
update responsetime_unit set
  netgeom=st_setsrid(st_pointfromtext('POINT (-95.6 36.0)'),4326),
  geom=st_setsrid(st_pointfromtext('POINT (-95.6 36.0)'),4326)
where descriptor = 'Unit 2';

update responsetime_unitnetwork set cost=-1 where unitid_id=
(select id from responsetime_unit where descriptor='Unit 2');

with s1 as (select o.gid,a.cost,row_number() over (order by a.cost) as seq
            from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost ' ||
'FROM responsetime_roadnetwork where area_id=1',
nn((select geom from responsetime_unit where descriptor='Unit 2'),
   0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 20, true, true) a
JOIN responsetime_roadnetwork o ON a.id1 = o.source order by a.cost)
update responsetime_unitnetwork n
set cost=s1.cost, sequence=s1.seq
from s1 where n.roadsegment_id=s1.gid and unitid_id=
(select id from responsetime_unit where descriptor='Unit 2');


--update final grid
--works, but leaves open areas open and is actually off by a bit
update responsetime_responsegrid set mintime=99;
with s1 as (select sequence, u.cost, nrgrid_id
from responsetime_unitnetwork u
  join responsetime_roadnetwork n on roadsegment_id=gid)
update responsetime_responsegrid g
set mintime=cost
  from s1
where (mintime>cost or mintime<=0)
and s1.nrgrid_id=g.id;


--delete from responsetime_unitnetwork
--delete from responsetime_unitnetwork where unitid_id in (select id from responsetime_unit where descriptor='Unit 1')
insert into responsetime_unitnetwork (sequence,cost,roadsegment_id,unitid_id)
select seq, a.cost, gid, (select id from responsetime_unit where descriptor='Unit 1')
from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM responsetime_roadnetwork where area_id=1 order by gid',
nn((select geom from responsetime_unit where descriptor='Unit 1'), 0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 20, true, true) a
JOIN responsetime_roadnetwork o ON a.id1 = o.source order by seq;

--update unit
update responsetime_unit set
  netgeom=st_setsrid(st_pointfromtext('POINT (-96.1 36.3)'),4326),
  geom=st_setsrid(st_pointfromtext('POINT (-96.1 36.3)'),4326)
where descriptor = 'Unit 2';
vacuum analyze;
--delete from responsetime_unitnetwork where unitid_id in (select id from responsetime_unit where descriptor='Unit 2')
insert into responsetime_unitnetwork (sequence,cost,roadsegment_id,unitid_id)
select seq, a.cost, gid, (select id from responsetime_unit where descriptor='Unit 2')
from pgr_drivingDistance(
'SELECT source as id, source, target, cost, x1, y1,x2, y2, reverse_cost FROM responsetime_roadnetwork where area_id=1 order by gid',
nn((select geom from responsetime_unit where descriptor='Unit 2'), 0.005, 2, 20,'responsetime_roadnetwork','source','geom'), 20, true, true) a
JOIN responsetime_roadnetwork o ON a.id1 = o.source order by seq;

--<10s!
update responsetime_responsegrid set mintime=99;
update responsetime_responsegrid rg
set mintime=u.cost
  from responsetime_unitnetwork u
  join responsetime_roadnetwork n on roadsegment_id=gid
  join responsetime_grid g on g.poly && n.geom
where (mintime>u.cost)
and g.id=rg.gridpoint_id
and unitid_id=93 ;

--in (select id from responsetime_unit)

select * from responsetime_responsegrid where mintime>0 order by gridpoint_id;
select * from responsetime_grid g join responsetime_responsegrid r on g.id=r.gridpoint_id;
select * from responsetime_roadnetwork;
select * from responsetime_unitnetwork where roadsegment_id=37454;
select * from responsetime_unitnetwork where unitid_id=2 order by cost;
--delete from responsetime_unitnetwork

select * from responsetime_unit;
select unitid_id, count(*) from responsetime_unitnetwork group by unitid_id;
select * from responsetime_unitnetwork where unitid_id=99 order by cost;

update responsetime_responsegrid rg set mintime=-1
from responsetime_roadnetwork rn
join responsetime_grid g on g.poly && rn.geom
where rg.gridpoint_id=g.id;
