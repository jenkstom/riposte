#!/usr/bin/env python3

import os
#import psycopg2

#osmosis from ubuntu packages

areaname = "tulsa"
inputfile = "oklahoma-latest.osm.pbf"
minlong = -95.4
maxlong = -96.4
minlat = 35.75
maxlat = 36.41

limitpoly = str.format("""{0}
1
  {2} {4}
  {1} {4}
  {1} {3}
  {2} {3}
  {2} {4}
  END
END
""", areaname, minlong, maxlong, minlat, maxlat)

print(limitpoly)
f = open('limit.poly', 'w')
f.write(limitpoly)
f.close()

extractcmd = str.format("""osmosis --read-pbf file="{0}" \
  --bounding-polygon file="limit.poly" \
  --write-xml file="{1}.osm" """, inputfile, areaname)

print(extractcmd)
os.system(extractcmd)

#load road network from OSM data based on coverage polygon
#osm2pgrouting from: https://github.com/pgRouting/osm2pgrouting.git

schema = os.environ["FRDB_SCHEMA_NAME"]
user = os.environ["FRDB_USERNAME"]
pword = os.environ["FRDB_PASSWORD"]
host = os.environ["FRDB_HOST"]
port = os.environ["FRDB_PORT"]

if port == "":
  port = "5432"

routecmd = str.format("osm2pgrouting --file {0} --prefix pgr_ --dbname {1} --username {2} --password {3} --host {4} --clean",
                      areaname+".osm", schema, user, pword, host)

print(routecmd)
os.system(routecmd)



#generate grid based on coverage polygon
#processing.runalg("qgis:creategrid",1,"-96.4, -95.4, 35.75, 36.41",0.01,0.01,"EPSG:4326",None)


