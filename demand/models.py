from django.db import models
from django.contrib.gis.db import models
from responsetime.models import *

## demand

class DemandClass(models.Model):
    """
    Classes to categorize demand type. For instance, cardiac call, pediatric, burn, etc.
    """
    ClassDescription = models.CharField("Class description", max_length=999)


class DemandGrid(models.Model):
    """
    A set of gridpoints (matching the main grid) with a probability per hour of having a call
    """
    ClassType = models.ForeignKey(DemandClass)
    GridPoint = models.ForeignKey(Grid)
    probability = models.FloatField("Probability per hour of call")

