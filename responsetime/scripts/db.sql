--create database fleetres;
--alter database fleetres owner to fleetres;

--create extension postgis;
--create extension pgrouting;

-- Create required type
--DROP   TYPE IF EXISTS T_Grid CASCADE;
CREATE TYPE T_Grid AS (
  gcol  int4,
  grow  int4,
  geom geometry
);
-- Drop function is exists
--DROP FUNCTION IF EXISTS ST_RegularGrid(geometry, NUMERIC, NUMERIC, BOOLEAN);
-- Now create the function
CREATE OR REPLACE FUNCTION ST_RegularGrid(p_geometry   geometry,
  p_TileSizeX  NUMERIC,
  p_TileSizeY  NUMERIC,
  p_point      BOOLEAN DEFAULT TRUE)
  RETURNS SETOF T_Grid AS
  $BODY$
    DECLARE
       v_mbr   geometry;
       v_srid  int4;
       v_halfX NUMERIC := p_TileSizeX / 2.0;
       v_halfY NUMERIC := p_TileSizeY / 2.0;
       v_loCol int4;
       v_hiCol int4;
       v_loRow int4;
       v_hiRow int4;
       v_grid  T_Grid;
    BEGIN
       IF ( p_geometry IS NULL ) THEN
          RETURN;
       END IF;
       v_srid  := ST_SRID(p_geometry);
       v_mbr   := ST_Envelope(p_geometry);
       v_loCol := trunc((ST_XMIN(v_mbr) / p_TileSizeX)::NUMERIC );
       v_hiCol := CEIL( (ST_XMAX(v_mbr) / p_TileSizeX)::NUMERIC ) - 1;
       v_loRow := trunc((ST_YMIN(v_mbr) / p_TileSizeY)::NUMERIC );
       v_hiRow := CEIL( (ST_YMAX(v_mbr) / p_TileSizeY)::NUMERIC ) - 1;
       FOR v_col IN v_loCol..v_hiCol Loop
         FOR v_row IN v_loRow..v_hiRow Loop
             v_grid.gcol := v_col;
             v_grid.grow := v_row;
             IF ( p_point ) THEN
               v_grid.geom := ST_SetSRID(
                                 ST_MakePoint((v_col * p_TileSizeX) + v_halfX,
                                              (v_row * p_TileSizeY) + V_HalfY),
                                 v_srid);
             ELSE
               v_grid.geom := ST_SetSRID(
                                 ST_MakeEnvelope((v_col * p_TileSizeX),
                                                 (v_row * p_TileSizeY),
                                                 (v_col * p_TileSizeX) + p_TileSizeX,
                                                 (v_row * p_TileSizeY) + p_TileSizeY),
                                 v_srid);
             END IF;
             RETURN NEXT v_grid;
         END Loop;
       END Loop;
    END;
    $BODY$
LANGUAGE plpgsql IMMUTABLE
COST 100
ROWS 1000;

insert into responsetime_area (name) values ('Tulsa');

--create grid
insert into responsetime_grid ("column", row, area_id, poly, point)
select gcol, grow, (select id from responsetime_area where name='Tulsa'),
  st_setsrid(geom,4326) as poly, st_centroid(st_setsrid(geom,4326)) as pnt
from ST_RegularGrid((select st_extent(the_geom) from pgr_ways), 0.005, 0.005, false);

--fill responsegrid table
insert into responsetime_responsegrid (gridpoint_id,mintime)
    select id,-1 from responsetime_grid;

--delete from responsetime_roadnetwork
insert into responsetime_roadnetwork
(gid,class_id,length,length_m,name,source,target,x1,x2,y1,y2,cost,reverse_cost,
cost_s,reverse_cost_s,oneway,rule,maxspeed_forward,maxspeed_backward,osm_id,source_osm,
target_osm, geom,area_id)
select gid,class_id,length,length_m,name,source,target,x1,x2,y1,y2,cost,reverse_cost,
cost_s,reverse_cost_s,one_way,rule,maxspeed_forward,maxspeed_backward,osm_id,source_osm,
target_osm, the_geom,1  from pgr_ways w;

--fix costs. _s=seconds, otherwise minutes
update responsetime_roadnetwork SET
  cost_s=(length_m/maxspeed_forward)*3.6,
  reverse_cost_s = (length_m/maxspeed_backward)*3.6,
  cost=(length_m/maxspeed_forward)*0.06,
  reverse_cost = (length_m/maxspeed_backward)*0.06;

--for one way streets set time to *10 if going backward
--Yes, ambulances can drive backwards on one-way streets...
update responsetime_roadnetwork SET
  reverse_cost=reverse_cost*10,
  reverse_cost_s=reverse_cost_s*10
  where oneway=1;




create or replace function
  nn(nearTo                   geometry
   , initialDistance          real
   , distanceMultiplier       real
   , maxPower                 integer
   , nearThings               text
   , nearThingsIdField        text
   , nearThingsGeometryField  text)
 returns integer as $$
declare
  sql     text;
  result  integer;
begin
  sql := ' select ' || quote_ident(nearThingsIdField)
      || ' from '   || quote_ident(nearThings)
      || ' where st_dwithin($1, '
      ||   quote_ident(nearThingsGeometryField) || ', $2 * ($3 ^ $4))'
      || ' order by st_distance($1, ' || quote_ident(nearThingsGeometryField) || ')'
      || ' limit 1';
  for i in 0..maxPower loop
     execute sql into result using nearTo              -- $1
                                , initialDistance     -- $2
                                , distanceMultiplier  -- $3
                                , i;                  -- $4
    if result is not null then return result; end if;
  end loop;
  return null;
end
$$ language 'plpgsql' stable;


--configure nearest gridpoint for each road segment (slow!)
--update responsetime_roadnetwork rn set nrgrid_id=
--nn(geom, 0.005, 2, 10, 'responsetime_grid','id','point');

--(SELECT id FROM responsetime_grid g ORDER BY ST_Distance(g.point,rn.geom) ASC LIMIT 1);

--select * from responsetime_roadnetwork



