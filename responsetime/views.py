from django.shortcuts import render

# Create your views here.


#Update ambulance position
def update_position(request, id, lat=0, lon=0, timestamp="", hdop=0, altitude=0,
                    speed=0, heading=0, bearing=0, battery=0, vacc=0, hacc=0):
    """

    :param request:
    :param id:
    :param lat:
    :param lon:
    :param timestamp:
    :param hdop:
    :param altitude:
    :param speed:
    :param heading: compass heading
    :param bearing: alternate for heading
    :param battery:
    :param vacc:
    :param hacc:
    :return:
    """
    #call stored procedure
    #TODO: modify stored procedure to only update position if it has moved some
    # minimal distance from the position used to calculate network

    #insert record into update queue table
