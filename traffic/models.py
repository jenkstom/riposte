from django.db import models
from django.contrib.gis.db import models
from responsetime.models import *

#traffic model

#a traffic model is a list of RoadNetwork cost factors


class TrafficModel(models.Model):
  """
  A name for a collection of network modifications

  How to setup a schedule???
  """
  name = models.CharField("Traffic model")
  hour = models.IntegerField("Hour model applies to", null=True)
  weekday = models.IntegerField("Day of week", null=True)
  month = models.IntegerField("Month", null=True)
  season = models.IntegerField("Season", null=True)


class TrafficNetwork(models.Model):
  """
  A list of modifications to specific roadnetwork costs
  """
  trafficmodel = models.ForeignKey(TrafficModel)
  roadsegment = models.ForeignKey(RoadNetwork)
  factor = models.FloatField("Factor modification", blank=True, null=True)


